from flask import render_template, request, redirect, flash, url_for
from flaskr import app, db
from flaskr.Database.models import User, AdPublisher, AdProvider
from flaskr.Forms import SignUpForm, SignInForm
from flask_login import login_user, logout_user, login_required, current_user


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    """registers new user"""
    form = SignUpForm(request.form)
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    if request.method == 'POST' and form.validate_on_submit():
        acc_type = form.acc_type.data

        # create user
        user = User(username=form.username.data, email=form.email.data, account_type=acc_type,
                    pass_hash=User.create_password(form.password.data), full_name=form.full_name.data)
        db.session.add(user)
        db.session.commit()

        if acc_type == 2:  # create publisher if account type is publisher
            ad_publisher = AdPublisher(user_id=user.id, name=user.username)
            db.session.add(ad_publisher)
            db.session.commit()
        elif acc_type == 3:  # create provider if account type is provider
            ad_provider = AdProvider(user_id=user.id, name=user.username)
            db.session.add(ad_provider)
            db.session.commit()

        return redirect(url_for('signin'))
    return render_template('auth/signup.html', form=form)


@app.route('/signin', methods=['GET', 'POST'])
def signin():
    """Auth for user"""
    form = SignInForm(request.form)
    if request.method == 'POST' and form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and user.check_password(form.password.data):
            login_user(user, remember=form.remember_me.data)
            return redirect(url_for('home'))
        flash('Invalid email or password')
    return render_template('auth/signin.html', form=form)


@app.route('/sign-out', methods=['GET'])
@login_required
def sign_out():
    """Exit"""
    logout_user()
    return redirect(url_for('home'))
