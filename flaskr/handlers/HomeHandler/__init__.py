from flaskr import app
from flask_login import current_user, login_required
from flask import redirect, url_for, request, render_template


@app.route('/', methods=['GET'])
@login_required
def home():
    kwargs = {
        'path': request.path,
        'current_user_acc_type': current_user.account_type
    }
    return render_template('home.html', **kwargs)
