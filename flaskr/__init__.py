import os

from flask import Flask
from flask_wtf.csrf import CSRFProtect
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

from typing import Tuple


init = (CSRFProtect(),
        )


def create_app() -> Tuple[Flask, SQLAlchemy, LoginManager]:
    """application factory"""

    app = Flask(__name__, instance_relative_config=True)
    for module in init:
        module.init_app(app)

    app.config.from_object(os.environ['APP_CONFIG'])
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    db = SQLAlchemy(app)
    login = LoginManager(app)
    login.login_view = 'signin'

    @app.route('/status')
    def status():
        return {"DEBUG": int(os.environ.get("DEBUG", 0)), 'user': os.environ.get('DB_USER'),
                'password': os.environ.get('PASS'),
                'host': os.environ.get('HOST'),
                'database': os.environ.get('DB',)}
    return app, db, login


app, db, login = create_app()
from flaskr.handlers import HomeHandler, AuthHandler, AccountHandler, AdUnitsHandler, ControlPanelHandler
