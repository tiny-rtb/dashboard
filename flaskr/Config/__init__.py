import os
from flaskr.config import config
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'this-really-needs-to-be-changed'
    SQLALCHEMY_DATABASE_URI = f"postgresql+psycopg2://{os.environ.get('DB_USER', config['postgres']['user'])}:{os.environ.get('PASS', config['postgres']['pass'])}@{os.environ.get('HOST', config['postgres']['host'])}/{os.environ.get('DB', config['postgres']['database'])}"


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
