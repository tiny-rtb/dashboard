# each account with specific type has different access type
# admins have max access
# ToDO - think how restrict all roles

from flaskr import app, db
from flask_login import current_user, login_required
from flask import redirect, url_for, render_template, request
from flaskr.Database.models import User, AdProvider
from flaskr.Forms import ProviderAccountEndpointForm


@app.route('/account', methods=['GET', 'POST'])
@login_required
def account():
    """account page for user"""
    user = User.query.get(int(current_user.id))
    if current_user.account_type == 3:
        form = ProviderAccountEndpointForm(request.form)
        if request.method == 'POST' and form.validate_on_submit():
            ad_provider = AdProvider.query.filter_by(user_id=current_user.id).first()
            ad_provider.endpoint = form.endpoint.data
            ad_provider.is_test = form.is_test.data
            ad_provider.is_active = form.is_active.data
            db.session.commit()
    else:
        form = None

    kwargs = {
        'path': request.path,
        'full_name': user.full_name,
        'username': user.username,
        'email': user.email,
        'acc_type': user.acc_type.name,
        'current_user_acc_type': current_user.account_type,
        'provider_info': user.ad_providers,
        'form': form
    }
    return render_template('account.html', **kwargs)


@app.route('/account/providers', methods=['GET'])
@login_required
def providers():
    return "Page with providers"


@app.route('/account/publishers', methods=['GET'])
@login_required
def publishers():
    return "Page with publishers"


@app.route('/account/websites', methods=['GET'])
@login_required
def websites():
    return "Page with websites"
