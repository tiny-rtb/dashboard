from flask_wtf import FlaskForm, RecaptchaField
from wtforms import PasswordField, StringField, SubmitField, SelectField, BooleanField, TextAreaField
from wtforms.fields.html5 import EmailField
from wtforms.validators import Email, DataRequired, EqualTo, Length, ValidationError, NumberRange, InputRequired
from flaskr.Database.models import User


class SignUpForm(FlaskForm):
    full_name = StringField('Full name')
    username = StringField('Username', validators=[DataRequired()])
    email = EmailField('Email', validators=[Email(), DataRequired(), Length(min=6, max=35)])
    password = PasswordField('Password', validators=[DataRequired(),
                                          EqualTo('confirm', message='Passwords must match'),
                                          Length(min=8)])
    confirm = PasswordField('Confirm password')
    acc_type = SelectField('Account type', validators=[NumberRange(min=1)],
                           choices=[(0, 'Account type'), (1, 'Network'), (2, 'Publisher'), (3, 'Provider')], coerce=int)
    submit = SubmitField('Submit')
    # recaptcha = RecaptchaField()

    def validate_email(self, email):
        """checks for email uniqueness"""
        if User.query.filter_by(email=email.data).first() is not None:
            raise ValidationError("User with this email already exists")

    def validate_username(self, username):
        if User.query.filter_by(username=username.data).first() is not None:
            raise ValidationError("User with this name already exists")


class SignInForm(FlaskForm):
    email = EmailField('Email', validators=[Email(), DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember me')
    submit = SubmitField('Sign In')


class AdUnitForm(FlaskForm):
    name = StringField('Ad unit name', [InputRequired()])
    ad_publisher = SelectField('Ad publisher', coerce=int)
    format = SelectField('Format', coerce=int, choices=[(-1, 'Format'), (0, 'Other'), (1, '728x90'),
                                                        (2, '320x50'), (3, '300x250'),],
                         validators=[NumberRange(min=0)])
    code = TextAreaField('Code', validators=[InputRequired()])
    is_active = BooleanField('Is active')
    is_test = BooleanField('Is test')
    submit = SubmitField('Submit')


class EditAdUnit(FlaskForm):
    name = StringField('Ad unit name')
    ad_publisher = StringField('Ad publisher')
    format = StringField('Format')
    endpoint_hash = StringField('Endpoint')
    javascript = TextAreaField('Code', validators=[InputRequired()])
    is_active = BooleanField('Is active')
    is_test = BooleanField('Is test')
    submit = SubmitField('Submit')


class ProviderAccountEndpointForm(FlaskForm):
    endpoint = StringField('Endpoint')
    is_active = BooleanField('Is active')
    is_test = BooleanField('Is test')
    submit = SubmitField('Change endpoint')


class ControlForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Submit')

