import flask_sqlalchemy as sa

from typing import Dict, Any
from flaskr import db, login
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from flask_sqlalchemy import Model


class BaseModel(Model):

    def __repr__(self) -> str:
        return self._repr(id=self.id)

    def _repr(self, **fields: Dict[str, Any]) -> str:
        """
        Helper for __repr__
        """
        field_strings = []
        at_least_one_attached_attribute = False
        for key, field in fields.items():
            try:
                field_strings.append(f'{key}={field!r}')
            except sa.orm.exc.DetachedInstanceError:
                field_strings.append(f'{key}=DetachedInstanceError')
            else:
                at_least_one_attached_attribute = True
        if at_least_one_attached_attribute:
            return f"<{self.__class__.__name__}({','.join(field_strings)})>"
        return f"<{self.__class__.__name__} {id(self)}>"


class AccountTypes(db.Model, BaseModel):

    __tablename__ = 'account_types'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)

    def __repr__(self):
        return self._repr(id=self.id, name=self.name)


class User(UserMixin, db.Model, BaseModel):

    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    full_name = db.Column(db.String(255), unique=False, nullable=True)
    username = db.Column(db.String(255), unique=True, nullable=False)
    email = db.Column(db.String(512), unique=True, nullable=False)
    pass_hash = db.Column(db.String(512), unique=False, nullable=False)
    is_active = db.Column(db.Boolean, default=True, nullable=False)
    account_type = db.Column(db.Integer, db.ForeignKey('account_types.id'), nullable=False)
    acc_type = db.relation('AccountTypes', backref='User', lazy=True)
    ad_publishers = db.relation('AdPublisher', backref='User', lazy=True)
    ad_providers = db.relation('AdProvider', backref='User', lazy=True)

    @staticmethod
    def create_password(password: str) -> str:
        return generate_password_hash(password, 'sha256')

    def check_password(self, password: str) -> bool:
        return check_password_hash(self.pass_hash, password)

    @staticmethod
    @login.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    def __repr__(self):
        return self._repr(id=self.id,
                          username=self.username,
                          email=self.email,
                          acc_type=self.acc_type
                          )


class AdPublisher(db.Model, BaseModel):

    __tablename__ = 'ad_publishers'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    is_active = db.Column(db.Boolean, default=True, nullable=False)
    user = db.relation('User', backref='AdPublisher', lazy=True)
    ad_units = db.relation('AdUnit', backref='AdPublisher', lazy=True)

    def __repr__(self):
        return self._repr(id=self.id, name=self.name, user=self.user)


class AdProvider(db.Model, BaseModel):

    __tablename__ = 'ad_providers'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)
    endpoint = db.Column(db.String(512), unique=True, nullable=True)
    is_active = db.Column(db.Boolean, default=True, nullable=False)
    is_test = db.Column(db.Boolean, default=False, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    user = db.relation('User', backref='AdProvider', lazy=True)

    def __repr__(self):
        return self._rerp(id=self.id, name=self.name, is_active=self.is_active, endpoint=self.endpoint,
                          user=self.user)


class AdUnit(db.Model, BaseModel):

    __tablename__ = 'ad_units'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)
    endpoint_hash = db.Column(db.String(255), unique=True, nullable=False)
    is_active = db.Column(db.Boolean, nullable=False)
    is_test = db.Column(db.Boolean, nullable=False)
    format_id = db.Column(db.Integer, db.ForeignKey('ad_unit_format.id'), unique=False, nullable=False)
    javascript = db.Column(db.Text, unique=False, nullable=False)
    creation_date = db.Column(db.Date, unique=False, nullable=True)
    ad_pub_id = db.Column(db.Integer, db.ForeignKey('ad_publishers.id'), nullable=False)
    ad_publisher = db.relation('AdPublisher', backref='AdUnit', lazy=True)
    format = db.relation('AdUnitFormat', backref='AdUnit', lazy=True)

    def __repr__(self):
        return self._rerp(id=self.id, name=self.name, is_active=self.is_active, is_test=self.is_test,
                          ad_pub=self.ad_publisher)


class AdUnitFormat(db.Model, BaseModel):

    __tablename__ = 'ad_unit_format'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(10), unique=True, nullable=False)
