import os

from configparser import ConfigParser


config = ConfigParser()
if os.environ.get("DEBUG"):
    config.read('config_develop.ini')
else:
    config.read('config_prod.ini')
