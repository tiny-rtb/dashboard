import requests
import json

from flask import render_template, request, redirect, url_for
from flaskr import app, db
from flask_login import current_user
from ...Forms import ControlForm
from typing import Dict


@app.route('/control', methods=['GET', 'POST'])
def control():
    """send request to control panel (master) to fetch all active instances (slaves)"""

    if current_user.account_type != 0:
        return redirect(url_for('home'))

    form = ControlForm(request.form)
    instances = dict()
    if request.method == 'POST' and form.validate_on_submit():
        instances = current_instances(current_user.email, form.password.data)
    return render_template('control_panel.html', form=form, instances=instances)


@app.route('/update_cache')
@app.route('/update_cache/<ip>')
def update_cache(ip=None):
    """sends request to cache manager with new cache data"""

    req = dict()
    if ip:  # if IP in URL - update cache of specific instance
        req['ip'] = ip
    # ToDo - find a way to pass here new data for cache
    # ToDo - find a way to auth in cache manager
    resp = requests.post('http://127.0.0.1:8085/control/1', data=json.dumps(req))


def current_instances(email: str, password: str) -> Dict:
    """connects to the cache server and retrieves all working instances of Ad Exchanger"""
    # ToDO - change way of auth
    response = requests.post('http://127.0.0.1:8085/control', data=json.dumps({'email': email, 'password': password}))
    if response.status_code == 200:
        return response.json()
    else:
        return {}
