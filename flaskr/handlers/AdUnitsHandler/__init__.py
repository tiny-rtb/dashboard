from flaskr.Forms import AdUnitForm, EditAdUnit
from datetime import datetime
from uuid import uuid4
from flaskr import app, db
from flask import redirect, url_for, render_template, request
from flaskr.Database.models import AdUnit, AdPublisher
from flask_login import current_user, login_required


@app.route('/ad-units', methods=['GET'])
@login_required
def ad_units():
    """displays list of ad units based on account type"""
    all_ad_units = list()
    if current_user.account_type == 2:  # ad units for specific publisher
        all_ad_units = AdPublisher.query.filter_by(user_id=current_user.id).all()[0].ad_units
    elif current_user.account_type == 0:  # all ad units for admin
        all_ad_units = AdUnit.query.all()
    kwargs = {
        'path': request.path,
        'current_user_acc_type': current_user.account_type
    }
    return render_template('ad_units.html', all_ad_units=all_ad_units, **kwargs)


@app.route('/ad-units/new', methods=['GET', 'POST'])
@login_required
def new_ad_unit():
    """creates new ad unit"""
    form = AdUnitForm(request.form)
    if current_user.account_type == 2:  # publisher data for specific publisher
        pubs = [AdPublisher.query.filter_by(user_id=current_user.id).first()]
    elif current_user.accout_type == 0:  # all publishers for admin
        pubs = AdPublisher.query.all()
    form.ad_publisher.choices = [(pub.id, pub.name) for pub in pubs]

    if request.method == 'POST' and form.validate_on_submit():
        endpoint_hash = str(uuid4())
        while AdUnit.query.filter_by(endpoint_hash=endpoint_hash).all():
            endpoint_hash = str(uuid4())
        ad_unit_instance = AdUnit(name=form.name.data, format_id=form.format.data, javascript=form.code.data,
                                  creation_date=datetime.now(), ad_pub_id=form.ad_publisher.data,
                                  is_test=form.is_test.data, is_active=form.is_active.data, endpoint_hash=endpoint_hash)
        db.session.add(ad_unit_instance)
        db.session.commit()
        return redirect(url_for('ad_unit', ad_unit_id=ad_unit_instance.id))
    kwargs = {
        'path': request.path,
        'current_user_acc_type': current_user.account_type
    }
    return render_template('new_ad_unit.html', form=form, **kwargs)


@app.route('/ad-unit/<ad_unit_id>', methods=['GET', 'POST'])
@login_required
def ad_unit(ad_unit_id):
    """displays info for specific ad unit"""
    # ToDo - add full endpoint URL
    ad_unit_ = AdUnit.query.get(int(ad_unit_id))
    form = EditAdUnit(obj=ad_unit_)
    if request.method == 'POST' and form.validate_on_submit():
        ad_unit_.is_active = form.is_active.data
        ad_unit_.is_test = form.is_test.data
        ad_unit_.javascript = form.javascript.data
        db.session.commit()
    kwargs = {
        'path': request.path,
        'current_user_acc_type': current_user.account_type
    }
    return render_template('ad_unit.html', form=form, **kwargs)